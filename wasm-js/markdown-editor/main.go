package main

import (
	"syscall/js"
	"github.com/russross/blackfriday/v2"
)

func renderMarkdown(this js.Value, p []js.Value) interface{} {
	input := p[0].String()
	output := blackfriday.Run([]byte(input))
	return js.ValueOf(string(output))
}

func main() {
	c := make(chan struct{}, 0)
	js.Global().Set("renderMarkdown", js.FuncOf(renderMarkdown))
	<-c
}
