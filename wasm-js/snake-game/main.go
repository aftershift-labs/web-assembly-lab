package main

import (
	"math/rand"
	"syscall/js"
	"time"
)

var (
	canvas   js.Value
	ctx      js.Value
	width    int
	height   int
	gridSize = 20
	snake    = []Position{{X: 5, Y: 5}}
	food     = Position{X: 10, Y: 10}
	dx       = 1
	dy       = 0
	gameOver = false
)

type Position struct {
	X, Y int
}

func draw() {
	ctx.Call("clearRect", 0, 0, width, height)

	ctx.Set("fillStyle", "red")
	ctx.Call("fillRect", food.X*gridSize, food.Y*gridSize, gridSize, gridSize)

	ctx.Set("fillStyle", "green")
	for _, p := range snake {
		ctx.Call("fillRect", p.X*gridSize, p.Y*gridSize, gridSize, gridSize)
	}
}

func update() {
	if gameOver {
		return
	}

	head := snake[0]
	newHead := Position{X: head.X + dx, Y: head.Y + dy}

	// Duvarlardan geçme mantığı
	if newHead.X < 0 {
		newHead.X = width/gridSize - 1
	} else if newHead.X >= width/gridSize {
		newHead.X = 0
	}
	if newHead.Y < 0 {
		newHead.Y = height/gridSize - 1
	} else if newHead.Y >= height/gridSize {
		newHead.Y = 0
	}

	for _, p := range snake {
		if p == newHead {
			gameOver = true
			return
		}
	}

	snake = append([]Position{newHead}, snake...)

	if newHead == food {
		food = Position{X: rand.Intn(width / gridSize), Y: rand.Intn(height / gridSize)}
	} else {
		snake = snake[:len(snake)-1]
	}
}

func keyDownEvent(event js.Value) {
	key := event.Get("key").String()
	switch key {
	case "ArrowUp":
		if dy == 0 {
			dx = 0
			dy = -1
		}
	case "ArrowDown":
		if dy == 0 {
			dx = 0
			dy = 1
		}
	case "ArrowLeft":
		if dx == 0 {
			dx = -1
			dy = 0
		}
	case "ArrowRight":
		if dx == 0 {
			dx = 1
			dy = 0
		}
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())

	c := make(chan struct{}, 0)

	js.Global().Set("onkeydown", js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		keyDownEvent(args[0])
		return nil
	}))

	document := js.Global().Get("document")
	canvas = document.Call("getElementById", "canvas")
	ctx = canvas.Call("getContext", "2d")
	width = canvas.Get("width").Int()
	height = canvas.Get("height").Int()

	go func() {
		for {
			update()
			draw()
			time.Sleep(100 * time.Millisecond)
		}
	}()

	<-c
}
