# WASM Application Deployment to K8S

### Build Wasm + JS Image

```sh
docker build --push . -t ttl.sh/kwasm-snake-game-demo:v0.1.0

docker images ttl.sh/kwasm-snake-game-demo:v0.1.0
```

&nbsp;

### Deploy to K8S

```sh
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kwasm-snake-game-demo
spec:
  selector:
    matchLabels:
      app: kwasm-snake-game-demo
  replicas: 1
  template:
    metadata:
      labels:
        app: kwasm-snake-game-demo
    spec:
      containers:
      - name: kwasm-snake-game-demo
        image: ttl.sh/kwasm-snake-game-demo:v0.1.0
---
apiVersion: v1
kind: Service
metadata:
  name: kwasm-snake-game-demo
spec:
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  selector:
    app: kwasm-snake-game-demo
EOF
```

&nbsp;

```sh
kubectl port-forward --address=0.0.0.0 svc/kwasm-snake-game-demo :80
```

&nbsp;
