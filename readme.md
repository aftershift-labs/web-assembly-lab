# Web Assembly Lab

### Installing Requirements

- [Minikube](https://minikube.sigs.k8s.io/docs/start/?arch=%2Flinux%2Fx86-64%2Fstable%2Fbinary+download)
- [Docker](https://docs.docker.com/engine/install/)
- [Containerd](https://containerd.io/downloads/)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/)
- [Helm](https://helm.sh/docs/intro/install/)
- [Rustup](https://rustup.rs/)
- [GO](https://go.dev/doc/install)
- [TinyGO](https://tinygo.org/getting-started/install/)
- [Fermyon/Spin](https://developer.fermyon.com/spin/v2/install)
- [WasmEdge](https://wasmedge.org/docs/start/install)

&nbsp;

### Start Minikube

```sh
minikube start                       \
    --driver=docker                  \
    --container-runtime='containerd' \
    --addons=metrics-server          \
    --kubernetes-version=stable      \
    -n 1
```

&nbsp;


### Install KWASM to K8S

```sh
helm upgrade -i kwasm-operator kwasm-operator \
    --repo http://kwasm.sh/kwasm-operator     \
    -n kwasm                                  \
    --create-namespace                        \
    --wait                                 && \
kubectl annotate node --all kwasm.sh/kwasm-node=true

kubectl get po -n kwasm
```

&nbsp;
