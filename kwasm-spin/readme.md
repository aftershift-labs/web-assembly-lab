# WASM Application Deployment to K8S

### Build Spin Image

```sh
spin build

spin registry push ttl.sh/kwasm-spin-demo:v0.1.0
```

&nbsp;

### Apply RuntimeClass

```sh
# Spin

cat <<EOF | kubectl apply -f - 
apiVersion: node.k8s.io/v1
kind: RuntimeClass
metadata:
  name: spin
handler: spin
EOF

kubectl get runtimeclass
```

&nbsp;

### Deploy to K8S

```sh
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kwasm-spin-demo
spec:
  selector:
    matchLabels:
      app: kwasm-spin-demo
  replicas: 1
  template:
    metadata:
      labels:
        app: kwasm-spin-demo
    spec:
      runtimeClassName: spin
      containers:
      - name: kwasm-spin-demo
        image: ttl.sh/kwasm-spin-demo:v0.1.0
        command: ["/"]
---
apiVersion: v1
kind: Service
metadata:
  name: kwasm-spin-demo
spec:
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  selector:
    app: kwasm-spin-demo
EOF
```

&nbsp;

```sh
kubectl port-forward --address=0.0.0.0 svc/kwasm-spin-demo :80

# or

kubectl run \
    debugger-$(tr -dc [:lower:] < /dev/urandom | head -c4) \
    -ti -q --image=alpine \
    -- sh -c "apk add curl 1>/dev/null; echo; curl kwasm-spin-demo.default.svc.cluster.local:80; echo"
```

&nbsp;
