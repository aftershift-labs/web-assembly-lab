# WASM Application Deployment to K8S

### Build WasmEdge Image

```sh
docker build --push . --annotation "module.wasm.image/variant=compat-smart" -t ttl.sh/kwasm-wasmedge-demo:v0.1.0

docker images ttl.sh/kwasm-wasmedge-demo:v0.1.0
```

&nbsp;

### Apply RuntimeClass

```sh
# Wasmedge

cat <<EOF | kubectl apply -f - 
apiVersion: node.k8s.io/v1
kind: RuntimeClass
metadata:
  name: wasmedge
handler: wasmedge
EOF

kubectl get runtimeclass
```

&nbsp;

### Deploy to K8S

```sh
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kwasm-wasmedge-demo
spec:
  selector:
    matchLabels:
      app: kwasm-wasmedge-demo
  replicas: 1
  template:
    metadata:
      annotations:
        module.wasm.image/variant: compat-smart
      labels:
        app: kwasm-wasmedge-demo
    spec:
      runtimeClassName: wasmedge
      containers:
      - name: kwasm-wasmedge-demo
        image: ttl.sh/kwasm-wasmedge-demo:v0.1.0
        resources: {}
EOF
```

&nbsp;

```sh
kubectl logs -f $(kubectl get po | awk '/kwasm-wasmedge/{print $1}')
```

&nbsp;
